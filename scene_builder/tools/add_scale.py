def add_scale():
    while True:
        ret = input("Enter the original xyz:\n")
        x, y, z = ret.split(" ")
        
        sx = "${{{} * scale}}".format(x)
        sy = "${{{} * scale}}".format(y)
        sz = "${{{} * scale}}".format(z)

        print(sx, sy, sz)



if __name__ == "__main__":
    add_scale()