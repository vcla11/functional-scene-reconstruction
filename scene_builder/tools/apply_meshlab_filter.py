import meshlabxml as mlx
import os



def ply2obj(file_in, file_out):
    texture_obj_mesh = mlx.FilterScript(file_in=file_in, file_out=file_out)

    mlx.texture.per_triangle(
        texture_obj_mesh,
        sidedim=0,
        textdim=4096,
        border=2,
        method=1
    )

    mlx.transfer.vc2tex(
        texture_obj_mesh,
        tex_name='{}',
        tex_width=1024,
        tex_height=1024, 
        overwrite_tex=False, 
        assign_tex=False,
        fill_tex=True
    )

    texture_obj_mesh.run_script()


def transform_scale(file_in, file_out):
    texture_obj_mesh = mlx.FilterScript(file_in=file_in, file_out=file_out)

    mlx.texture.per_triangle(
        texture_obj_mesh,
        sidedim=0,
        textdim=4096,
        border=2,
        method=1
    )

    mlx.transfer.vc2tex(
        texture_obj_mesh,
        tex_name='{}',
        tex_width=1024,
        tex_height=1024, 
        overwrite_tex=False, 
        assign_tex=False,
        fill_tex=True
    )

    texture_obj_mesh.run_script()


if __name__ == "__main__":
    file_in = "output/scenenn_001/assets/8.ply"
    file_out = "output/scenenn_001/assets/8.obj"

    ply2obj(file_in, file_out)
    transform_scale(file_in, file_out)
