import os
import sys
import glob

import numpy as np

import transformation as TF


MESHLAB_COLOR_TO_TEXTURE_TEMPLATE = "../assets/meshlab_scripts/vertex_color_to_texture.mlx"
MESHLAB_TEXTURE_FILE_TEMPLATE = "{}_texture.png"

MESHLAB_TRANSFORM_TEMPLATE = "../assets/meshlab_scripts/set_transform.mlx"

TMP_MESHLAB_SCRIPT = ".tmp_meshlab_filter_script.mlx"
MESHLAB_EXE_TEMPLATE = "meshlab.meshlabserver -i {} -o {} -m vc vn fc wt -s {}"

SHAPENET_DB = "/home/zeyu/Downloads/models"


class MeshlabServer(object):

    def __init__(self):
        pass


    def apply_transform(self, file_in, file_out, tf):
        # generating the meshlab script
        script = self.load_script_template_(MESHLAB_TRANSFORM_TEMPLATE)
        tf = tf.flatten()
        script = script.format(*tf)
        script_dir = self.dump_script_template_(script, TMP_MESHLAB_SCRIPT)

        input_dir = self.get_parent_dir_(file_in)
        output_dir = self.get_parent_dir_(file_out)

        cmd = MESHLAB_EXE_TEMPLATE.format(file_in, file_out, script_dir)

        print("Processing `{}`, waiting...".format(cmd))
        os.system(cmd)

        # remove temporary meshlab script
        os.system("rm {}".format(script_dir))

        mtl_file = "{}.mtl".format(file_out)
        with open(mtl_file, "r") as fin:
            for line in fin:
                if "png" in line or "jpg" in line or "jpeg" in line:
                    filename = line.strip('\n').split(' ')[-1]
                    cmd = "cp {}/{} {}/{}".format(input_dir, filename, output_dir, filename)
                    
                    if os.system(cmd) != 0:
                        print("[ERROR] Copy `{}`".format(cmd))
                        exit(1)

        if os.path.exists(file_out):
            print("[INFO] Save transformed CAD at: `{}`".format(file_out))
        else:
            print("[ERROR] Fail to transform CAD model `{}`".format(file_in))
            raise

    def convert_ply_to_obj(self, input_dir, output_dir, filename):
        # generating the meshlab script
        script = self.load_script_template_(MESHLAB_COLOR_TO_TEXTURE_TEMPLATE)
        texture_filename = MESHLAB_TEXTURE_FILE_TEMPLATE.format(filename)
        script = script.format(texture_filename)
        script_dir = self.dump_script_template_(script, TMP_MESHLAB_SCRIPT)

        file_in = "{}/{}.ply".format(input_dir, filename)
        file_out = "{}/{}.obj".format(output_dir, filename)

        cmd = MESHLAB_EXE_TEMPLATE.format(file_in, file_out, script_dir)

        print("Processing `{}`, waiting...".format(cmd))
        os.system(cmd)

        # mv meshlab generated texture file file to dump out directory
        os.system("mv {}/{} {}/".format(input_dir, texture_filename, output_dir))
        # remove temporary meshlab script
        os.system("rm {}".format(script_dir))

        if os.path.exists(file_out):
            print("[INFO] Convert .ply to .obj at: `{}`".format(file_out))
        else:
            print("[ERROR] Fail to convert .ply to .obj: `{}`".format(file_in))
            raise


    def load_script_template_(self, script_dir):
        script = ""

        with open(script_dir, "r") as fin:
            for line in fin:
                script += line
        
        return script

    
    def dump_script_template_(self, script, out_dir):
        with open(out_dir, "w") as fout:
            fout.write(script)

        return out_dir


    def get_parent_dir_(self, file_dir):
        parent_dir = "/".join(file_dir.split('/')[:-1])
        return parent_dir


def load_cad_transform(cad_tf_file):
    cad_tfs = {}

    with open(cad_tf_file, 'r') as fin:
        # skip the header line
        _ = fin.readline()

        for line in fin:
            tkns = line.strip('\n').replace('"', '').split(',')

            cid = tkns[0].split('.')[-1]
            tf_list = [float(i) for i in tkns[1:]]
            
            tf = np.array([
                tf_list[0:4],
                tf_list[4:8],
                tf_list[8:12],
                tf_list[12:16]
            ], dtype="float64")

            cad_tfs[cid] = tf

    return cad_tfs
        

def mesh_transform(cad_tf_file, output_dir):
    meshlab = MeshlabServer()
    cad_tfs = load_cad_transform(cad_tf_file)

    if os.system("mkdir -p {}".format(output_dir)) != 0:
        print("[ERROR] Cannot make folder: {}".format(output_dir))
        raise

    for cid, tf in cad_tfs.items():
        file_in = "{}/{}.obj".format(SHAPENET_DB, cid)
        file_out = "{}/{}.obj".format(output_dir, cid)
        
        meshlab.apply_transform(file_in, file_out, tf)

    # rename texture jpg files
    # store global original picture filenames
    # noted that different obj may use same picture when loading the texture
    original_pics = {}
    for cid, _ in cad_tfs.items():
        mtl_file = "{}/{}.obj.mtl".format(output_dir, cid)
        content = ""

        with open(mtl_file, "r") as fin:
            print("[INFO] Processing mtl file: {}".format(mtl_file))
            pic_dict = {}

            for line in fin:
                if "png" in line or "jpg" in line or "jpeg" in line:
                    tkns = line.strip('\n').split(' ')
                    filename, ext = tkns[-1].split('.')

                    # check global picture records
                    if "{}.{}".format(filename, ext) not in original_pics:
                        original_pics["{}.{}".format(filename, ext)] = True
                    
                    # check if same image file has been processed
                    if filename not in pic_dict:
                        new_filename = "{}_{}".format(cid, len(pic_dict))
                        pic_dict[filename] = new_filename

                        cmd = "cp {}/{}.{} {}/{}.{}".format(
                            output_dir, filename, ext, output_dir, new_filename, ext
                        )
                        if os.system(cmd) != 0:
                            print("[WARNING] Cannot execute cmd: {}".format(cmd))
                            raise

                    line = "{} {}.{}\n".format(" ".join(tkns[:-1]), pic_dict[filename], ext)
                
                content += line

        # update new mtl file
        with open(mtl_file, "w") as fout:
            fout.write(content)
    
    # clean all original picture files
    for file, _ in original_pics.items():
        cmd = "rm {}/{}".format(output_dir, file)
        if os.system(cmd) != 0:
            print("[ERROR] Fail to execute cmd: {}".format(cmd))
            raise


def gen_scaling_matrix(sx, sy, sz):
    tf = np.array([
        [sx, 0, 0, 0],
        [0, sy, 0, 0],
        [0, 0, sz, 0],
        [0, 0,  0, 1]
    ])

    return tf

def scale_mesh(input_file, output_dir):
    os.system("mkdir -p {}".format(output_dir))
    meshlab = MeshlabServer()

    filename = input_file.split('/')[-1]
    file_out = "{}/{}".format(output_dir, filename)

    scale = 0.3092968583106995
    translation = [-0.439551 * scale, 0.474092 * scale, 0.260048 * scale]
    origin_tf = TF.from_translation_rotation(translation, [0, 0, 0, 1])

    scale_tf = gen_scaling_matrix(0.3092968583106995, 0.3092968583106995, 0.3092968583106995)

    tf = np.dot(origin_tf, scale_tf)
    
    meshlab.apply_transform(input_file, file_out, tf)


def ply_to_obj(input_file, output_dir):
    os.system("mkdir -p {}".format(output_dir))
    meshlab = MeshlabServer()

    filename = input_file.split('/')[-1].split('.')[0]
    input_dir = "/".join(input_file.split('/')[:-1])
    
    meshlab.convert_ply_to_obj(input_dir, output_dir, filename)
    

if __name__ == "__main__":
    # if len(sys.argv) < 3:
    #     print("Need to specify the CAD transform directory, and the output directory")
    #     exit(1)
    
    # config_file, output_dir
    # mesh_transform(sys.argv[1], sys.argv[2])

    # input_file = "../output/scenenn_001/assets/interactive/microwave_0001/dof_rootd_Aa001_r.obj"
    # output_dir = "scenenn_001_scaled/interactive/microwave_0001"
    # scale_mesh(input_file, output_dir)

    input_file = "/home/zeyu/Dropbox/Research/_OnGoing/2021-ICRA-SLAM/Figures/Figure.5/PanoMap/225.ply"
    output_dir = "panomap_obj"
    ply_to_obj(input_file, output_dir)