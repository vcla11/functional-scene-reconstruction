import os
import json

import trimesh

import transformation as TF


def view_mesh(mesh_file):
    mesh = trimesh.load(mesh_file)
    mesh.show()


def view_scene(mesh_files):
    scene = trimesh.Scene()

    for file in mesh_files:
        mesh = trimesh.load(file)
        scene.add_geometry(mesh)

    scene.show()


def set_mesh_origin(mesh, translation, rotation):
    mesh = mesh.copy()

    tf_mat = TF.from_translation_rotation(translation, rotation)
    mesh.apply_transform(tf_mat)

    return mesh


def align_mesh(mesh):
    center = mesh.center_mass
    
    # set_mesh_origin will make a deep copy of the input mesh
    mesh = set_mesh_origin(mesh, -center, [0, 0, 0, 1])
    
    return mesh


def save_mesh(mesh, outfile):
    mesh.export(outfile)
    print("Mesh is saved at: {}".format(outfile))


def calc_align_transform(mesh_original_dir, mesh_aligned_dir):
    mesh_original = trimesh.load(mesh_original_dir, face_colors=[255, 0, 0, 128])
    mesh_aligned = trimesh.load(mesh_aligned_dir, face_colors=[0, 0, 255, 128])

    scale_factor = 1.0

    # tf = TF.from_translation_rotation([0, 0, 0], [ 0, 0, 0, 1 ])
    tf = TF.from_translation_rotation([0.025428, 0.196997, -0.07652585], [ 0, 0, 0.7071068, 0.7071068 ])
    
    # tf = [
    #     [2.220446049250313e-16, -0.9999999999999998, 0.0, 0.0],
    #     [0.9999999999999998, 2.220446049250313e-16, 0.0, 0.0],
    #     [0.0, 0.0, 1.0, 0.0],
    #     [0.0, 0.0, 0.0, 1.0],
    # ]

    mesh_original.apply_transform(tf)
    mesh_original.apply_scale(scale_factor)

    print(mesh_original.bounds)
    print(mesh_aligned.bounds)
    print("Difference\n")
    print(mesh_aligned.bounds - mesh_original.bounds)

    scene = trimesh.Scene([mesh_original, mesh_aligned])
    scene.show()

    # Format output of the JSON config
    mesh_name = mesh_original_dir.split('/')[-1].split('.')[0]

    json_str = ""
    json_str += "\"{}\": {{\n".format(mesh_name)
    json_str += "    \"scale\": {},\n".format(scale_factor)
    json_str += "    \"tf\": [\n"
    
    for r in tf:
        json_str += "    [{}, {}, {}, {}],\n".format(*r)

    json_str += "    ]\n"
    json_str += "}\n"

    print(json_str)


def scale_mesh(mesh_file, scale, out_dir):
    mesh = trimesh.load(mesh_file)
    mesh.apply_scale(scale)

    filename = mesh_file.split("/")[-1].split('.')[0]
    
    os.system("mkdir -p {}".format(out_dir))

    out_file = "{}/{}_{}.obj".format(out_dir, filename, scale)
    mesh.export(out_file)

    print("Mesh `{}` is saved at `{}`".format(mesh_file, out_file))
    

if __name__ == "__main__":
    # mesh = "/home/zeyu/Downloads/aligned_interactive/cabinet_0001.obj"
    # view_mesh(mesh)
    # view_scene(mesh_files)

    mesh_original_dir = "/home/zeyu/Workspace/Robot-CV-ROS/src/Robot-Vision-System/scene_builder/tools/unaligned_interactive/fridge_0003.obj"
    mesh_aligned_dir = "/home/zeyu/Downloads/aligned_interactive/fridge_0003.obj"

    # calc_align_transform(mesh_original_dir, mesh_aligned_dir)

    # mesh_file = "/home/zeyu/Workspace/Robot-CV-ROS/src/Robot-Vision-System/scene_builder/output/scenenn_001/assets/1.ply"
    meshes = [
        "interactive_obj_opened/fridge_0001_closed.obj",
        # "interactive_obj_opened/fridge_0001_opened_30.obj",
        "interactive_obj_opened/fridge_0001_opened_60.obj",
        "interactive_obj_opened/fridge_0001_opened_90.obj"
    ]
    out_dir = "fridge_0001"

    for m in meshes:
        scale_mesh(m, 2.09, out_dir)