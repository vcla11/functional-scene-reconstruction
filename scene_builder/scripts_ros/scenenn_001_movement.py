#!/usr/bin/env python
import rospy
import tf
from sensor_msgs.msg import JointState

JOINT_NAMES = [
    "Refrigerator_559_link_dof_rootd_Aa001_r_joint",    # door
    "Refrigerator_559_link_dof_rootd_Aa002_r_joint",    # door
    "Refrigerator_559_link_dof_rootd_Ba001_t_joint",
    "Refrigerator_559_link_dof_rootd_Ba002_t_joint",
    "Refrigerator_559_link_dof_rootd_Ba003_t_joint",
    "Microwave_522_link_dof_rootd_Aa001_r"
]

FIRDGE_DOOR_1_SEQ = [i * 0.01 for i in range(0, 140, 4)]
FIRDGE_DOOR_2_SEQ = [i * 0.01 for i in range(0, 155, 4)]
MICROWAVE_DOOR_SEQ = [i * 0.01 for i in range(0, 160, 4)]


# x, y, z, r, p, y
CHAIR_55_SEQ = [
    [-0.9258648753166199 - 0.0003 * i for i in range(0, 155, 5)],
    [0.42939046025276184 for i in range(0, 155, 5)],
    [0.5451453924179077 - 0.0018 * i for i in range(0, 155, 5)],
    [-1.5707963267948968 for i in range(0, 155, 5)],
    [-0.07705623334724376 + 0.0013 * i for i in range(0, 155, 5)],
    [0.0 for i in range(0, 155, 5)]
]

# x, y, z, r, p, y
CHAIR_475_SEQ = [
    [-1.5442392826080322 + 0.0003 * i for i in range(0, 155, 5)],
    [0.41335996985435486 for i in range(0, 155, 5)],
    [-0.5364120602607727 - 0.0018 * i for i in range(0, 155, 5)],
    [-1.5707962424883708 for i in range(0, 155, 5)],
    [-0.020118951415368812 - 0.0013 * i for i in range(0, 155, 5)],
    [-8.48108056756302e-10 for i in range(0, 155, 5)]
]


class SceneController(object):

    def __init__(self):
        self.br_ = tf.TransformBroadcaster()
        self.joint_pub_ = rospy.Publisher("joint_states", JointState, queue_size=1)
        
        self.joint_state_ = JointState()

        for name in JOINT_NAMES:
            self.joint_state_.name.append(name)
            self.joint_state_.position.append(0.0)
        
        # keep poblishing joint state until other component ready
        rate = rospy.Rate(20)
        for _ in range(100):
            self.joint_state_.header.stamp = rospy.Time.now()
            self.joint_pub_.publish(self.joint_state_)
            rate.sleep()


    def open_fridge_door_1(self):
        rate = rospy.Rate(20)

        positions = FIRDGE_DOOR_1_SEQ[:]

        for pos in positions:
            self.joint_state_.header.stamp = rospy.Time.now()
            self.joint_state_.position[0] = pos
            self.joint_pub_.publish(self.joint_state_)
            rate.sleep()

    
    def close_fridge_door_1(self):
        rate = rospy.Rate(20)

        positions = FIRDGE_DOOR_1_SEQ[:]
        # positions.reverse()
        
        for pos in reversed(positions):
            self.joint_state_.header.stamp = rospy.Time.now()
            self.joint_state_.position[0] = pos
            self.joint_pub_.publish(self.joint_state_)
            rate.sleep()


    def open_fridge_door_2(self):
        rate = rospy.Rate(20)

        positions = FIRDGE_DOOR_2_SEQ[:]

        for pos in positions:
            self.joint_state_.header.stamp = rospy.Time.now()
            self.joint_state_.position[1] = pos
            self.joint_pub_.publish(self.joint_state_)
            rate.sleep()

    
    def close_fridge_door_2(self):
        rate = rospy.Rate(20)

        positions = FIRDGE_DOOR_2_SEQ[:]
        # positions.reverse()
        
        for pos in reversed(positions):
            self.joint_state_.header.stamp = rospy.Time.now()
            self.joint_state_.position[1] = pos
            self.joint_pub_.publish(self.joint_state_)
            rate.sleep()


    def open_microwave_door(self):
        rate = rospy.Rate(20)

        positions = MICROWAVE_DOOR_SEQ[:]

        for pos in positions:
            self.joint_state_.header.stamp = rospy.Time.now()
            self.joint_state_.position[5] = pos
            self.joint_pub_.publish(self.joint_state_)
            rate.sleep()

    
    def close_microwave_door(self):
        rate = rospy.Rate(20)

        positions = MICROWAVE_DOOR_SEQ[:]
        # positions.reverse()
        
        for pos in reversed(positions):
            self.joint_state_.header.stamp = rospy.Time.now()
            self.joint_state_.position[5] = pos
            self.joint_pub_.publish(self.joint_state_)
            rate.sleep()


    def move_chair_55(self):
        rate = rospy.Rate(20)

        seq_len = len(CHAIR_55_SEQ[0])
        
        rate = rospy.Rate(20)
        for i in range(0, seq_len):
            self.br_.sendTransform(
                (CHAIR_55_SEQ[0][i], CHAIR_55_SEQ[1][i], CHAIR_55_SEQ[2][i]),
                tf.transformations.quaternion_from_euler(CHAIR_55_SEQ[3][i], CHAIR_55_SEQ[4][i], CHAIR_55_SEQ[5][i]),
                rospy.Time.now(),
                "Chair_55_link",
                "Floor_2_link"
            )
            rate.sleep()


    def move_chair_55_back(self):
        rate = rospy.Rate(20)

        seq_len = len(CHAIR_55_SEQ[0])
        
        rate = rospy.Rate(20)
        for i in reversed(range(0, seq_len)):
            self.br_.sendTransform(
                (CHAIR_55_SEQ[0][i], CHAIR_55_SEQ[1][i], CHAIR_55_SEQ[2][i]),
                tf.transformations.quaternion_from_euler(CHAIR_55_SEQ[3][i], CHAIR_55_SEQ[4][i], CHAIR_55_SEQ[5][i]),
                rospy.Time.now(),
                "Chair_55_link",
                "Floor_2_link"
            )
            rate.sleep()


    def move_chair_475(self):
        rate = rospy.Rate(20)

        seq_len = len(CHAIR_475_SEQ[0])
        
        rate = rospy.Rate(20)
        for i in range(0, seq_len):
            self.br_.sendTransform(
                (CHAIR_475_SEQ[0][i], CHAIR_475_SEQ[1][i], CHAIR_475_SEQ[2][i]),
                tf.transformations.quaternion_from_euler(CHAIR_475_SEQ[3][i], CHAIR_475_SEQ[4][i], CHAIR_475_SEQ[5][i]),
                rospy.Time.now(),
                "Chair_475_link",
                "Floor_2_link"
            )
            rate.sleep()


    def move_chair_475_back(self):
        rate = rospy.Rate(20)

        seq_len = len(CHAIR_475_SEQ[0])
        
        rate = rospy.Rate(20)
        for i in reversed(range(0, seq_len)):
            self.br_.sendTransform(
                (CHAIR_475_SEQ[0][i], CHAIR_475_SEQ[1][i], CHAIR_475_SEQ[2][i]),
                tf.transformations.quaternion_from_euler(CHAIR_475_SEQ[3][i], CHAIR_475_SEQ[4][i], CHAIR_475_SEQ[5][i]),
                rospy.Time.now(),
                "Chair_475_link",
                "Floor_2_link"
            )
            rate.sleep()


def print_menu():
    menu = ""

    menu += "0. Open fridge door 1\n"
    menu += "1. Close fridge door 1\n"
    menu += "2. Open fridge door 2\n"
    menu += "3. Close fridge door 2\n"
    menu += "4. Open microwave door\n"
    menu += "5. Close microwave door\n"
    menu += "6. Move chair_55\n"
    menu += "7. Move chair_55 back\n"
    menu += "8. Move chair_475\n"
    menu += "9. Move chair_475 back\n"
    
    print(menu)


def main():
    rospy.init_node("Scenenn_001_Movement")
    scene_ctrl = SceneController()

    menu_func = [
        scene_ctrl.open_fridge_door_1,
        scene_ctrl.close_fridge_door_1,
        scene_ctrl.open_fridge_door_2,
        scene_ctrl.close_fridge_door_2,
        scene_ctrl.open_microwave_door,
        scene_ctrl.close_microwave_door,
        scene_ctrl.move_chair_55,
        scene_ctrl.move_chair_55_back,
        scene_ctrl.move_chair_475,
        scene_ctrl.move_chair_475_back
    ]
    
    while not rospy.is_shutdown():
        print_menu()
        ret = int(input("Select action: "))
        
        menu_func[ret]()
            

if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass