# SceneBuilder ROS Package

This package provides function that convert a parse graph to URDF scene which could be further loaded into ROS, or some motion planning frameworks, e.g., Tesseract. 

## 1. Dependencies

Several dependencies need to be installed before using this package:

- **Meshlab** (version 2020.07): installation instruction could be found [here](https://snapcraft.io/install/meshlab/ubuntu).
- **Python packages**: use command `pip install -r requirements.txt` to install these required python dependencies. Noted that is you do not use a python virtual environment, `sudo` is required to install these dependencies.

To use this package, we need to configure our local CAD model database. One need to place

- The rigid CAD models under directory: `<scene-builder-pkg>/assets/rigid`
- The interactive CAD models under directory: `<scene-builder-pkg>/assets/interactive`

Once the configuration is done, we should following structure under the `assets` folder

```text
assets/
	rigid/
		xxx.obj
		xxx.mtl
		xxx.png
		...
	interactive/
		fridge_0001/
			xxx.obj
			xxx.mtl
			xxx.urdf
			xxx.xacro
			...
		fridge_0002/
		...
	meshlab_scripts/
		...
```

## 2. Usage

### 2.1 Input Format

The input should be organized in a folder where all materials are included to construct the URDF scene. The folder should have following structure

```text
<pg-scene-folder>/
	segments/
		xxx.ply
		...
	contact_graph.json
```

where the sub-folder `segments/` contains all segmented meshes, and `contact_graph.json` stores the parse graph in a JSON file.

## 2.2 scene_builder.py

We can use the script `scene_builder.py` to generate an URDF scene from a parse graph. Get into the `<scene_builder>` directory, use command `python scripts/scene_builder.py -h` get usage information:

```text
usage: Scene Builder [-h] -c, --src SRC [-o, --out OUT] [--name NAME]
                     [--vrgym VRGYM]

optional arguments:
  -h, --help     show this help message and exit
  -c, --src SRC  Input source
  -o, --out OUT  Output directory
  --name NAME    Scene name
  --vrgym VRGYM  Enable VRGym
```

For example:

```
python scripts/scene_builder.py -c input/scene_001 -o output --name=scene_001_urdf
```

The preceding command will read input from the directory `input/scene_001`, and dump the URDF scene into `output/scene_001_urdf`. Noted that `-o` option only specify the root directory of the output folder, and `--name` specifies the folder name of URDF scene. The final output directory is `<output-dir>/<scene-name>`.

*Noted that `-o` and `--name` are optional, the program automatically generate scene name according to the input.*



We also provide some [launch files](launch/) to visualize the constructed virtual interactive scene. For example, use

```bash
roslaunch scene_builder view_scene.launch scene:=scene_001_urdf
```

to visualize the scene whose name is `scene_001_urdf`. 

*Note: Please build this package before using the launch file to visualize the scene.*

