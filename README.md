# Functional Scene Reconstruction

[![ros-version](https://img.shields.io/badge/ROS-Melodic-blue)](http://wiki.ros.org/melodic) [![ubuntu-version](https://img.shields.io/badge/Ubuntu-18.04-blue)](http://releases.ubuntu.com/18.04/) ![python-version](https://img.shields.io/badge/Python-3.7%2B-blue)

A ROS-based robot visual perception system for functional scene reconstruction in dynamic environment. The system is built on three modules:
- A feature-based dynamic SLAM module based on [ORB-SLAM2](https://github.com/appliedAI-Initiative/orb_slam_2_ros)
- A panoptic segmentation module based on [Detectron2](https://github.com/facebookresearch/detectron2)
- An instance-aware tsdf semantic mapping module based on [Voxblox-plus-plus](https://github.com/ethz-asl/voxblox-plusplus)


## 1. Installation

### 1.1 Prerequisites

The majority of this project is developed based on Python 3.7, and is tested on Ubuntu 16.04(ROS Kinetic) and 18.04(ROS Melodic). This system depends on several 3rd-party modules which require **manually** installed in your operating system

- [Detectron2](https://github.com/facebookresearch/detectron2)
- [CUDA 10.0+](https://developer.nvidia.com/) and [cuDNN 7+](https://docs.nvidia.com/deeplearning/sdk/cudnn-install/index.html)


Here we provide a brief installation instructions. Any problems occurred while setting up the environment, you can refers to the [Trouble Shooting](#4-Trouble-Shooting) section.

**Step 1 (Optional): Installing python virtual environment**

We highly recommend using Python virtual environment to configure Python runtime of this project, which allows you isolating the runtime environment from your host system. Firstly, you need to install Python 3.7 in your system.

Then, install [virtualenv](https://virtualenv.pypa.io/en/stable/installation/)

```bash
sudo apt-get install python-pip python3-pip
sudo pip install virtualenv
sudo pip3 install virtualenv
```

Next, install [virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/install.html) which allows you using virtualenv in a more convenient way

```bash
sudo pip install virtualenvwrapper
```

Then add following line to your shell startup file (e.g., `.bashrc`, `.zshrc`, etc.)

```bash
export WORKON_HOME=$HOME/.virtualenvs
export PROJECT_HOME=<your-working-folder>
source /usr/local/bin/virtualenvwrapper.sh
```

After adding preceding lines, reload the startup file (e.g., run `source ~/.zshrc`)

Finally, we can create a Python virtual environment using Python 3.7 as default Python

```bash
mkvirtualenv <env-name> -p `which python3.7`
```

where `<env-name>` should be replaced by your desired name. Then you can activate your newly created environment by `workon <env-name>` and deactivate it by using `deactivate`.


**Step 2: Setup Nvidia Driver, CUDA and cuDNN**

Firstly, we install Nvidia Driver

```shell
sudo apt-get remove --purge '^nvidia-.*'
sudo apt-get install nvidia-430
```
If you are using Ubuntu 18.04, try

```shell
sudo apt-get remove --purge '^nvidia-.*'
sudo add-apt-repository ppa:graphics-drivers/ppa
sudo apt-get update
sudo apt-get install nvidia-340
```

Then reboot your system using `sudo reboot`. After rebooting validate the driver via

```shell
nvidia-smi
```

Next, we install CUDA toolkit. We use CUDA 10.0 in our system, which is available [here](https://developer.nvidia.com/cuda-downloads). We can also try the latest version, which is available [here](https://developer.nvidia.com/cuda-10.0-download-archive). Follow the official instruction, you can install the CUDA toolkit (*Note: you do NOT need to install the Nvidia driver again while installing the CUDA, since we already installed it.*). 

The installation instruction for cuDNN is available [here](https://docs.nvidia.com/deeplearning/sdk/cudnn-install/index.html).



**Step 3: Install Detectron2**

Firstly, install [pytorch](https://pytorch.org/get-started/locally/) as following

```shell
pip install torch torchvision
```

Next, install [pycocotools](https://github.com/cocodataset/cocoapi) as following

```shell
pip install cython
pip install 'git+https://github.com/cocodataset/cocoapi.git#subdirectory=PythonAPI'
```

Finally, build and install Detectron2 (make sure  your gcc & g++ $\geq$ 4.9 before running the command below)

```shell
pip install 'git+https://github.com/facebookresearch/detectron2.git'
```

*Noted: You often need to **rebuild** Detectron2 after reinstalling PyTorch.* 


### 1.2 Build the project

First of all, it should be noted that, we separate the Python 3-based modules from ROS by setting up a TCP-based communication mechanism. Thus, there's no need for setting up ROS to work with Python 3. However, we'll still talk about how to build a ROS project with Python 3.7 in case you need it in the future.


**Step 0: create and configure a catkin workspace**

This project requires 'catkin build' to build the source code, and we'll show how to configure a catkin workspace from scratch. If you already have a catkin workspace but using 'catkin_make', you need to first run 'catkin clean' and then follow the instructions to reconfigure it.

First, if you haven't had a catkin workspace, create one.

``` shell
cd <your-working-directory>
mkdir catkin_ws/src && cd catkin_ws
```

Then, initialize the workspace and configure it.

``` shell
catkin init
catkin config --extend /opt/ros/<your-ros-version> --merge-devel 
catkin config --cmake-args -DCMAKE_CXX_STANDARD=14 -DCMAKE_BUILD_TYPE=Release
```

**Step 1: download project in your ROS workspace and add dependencies with wstool**

Download this repository to your ROS workspace `src/` folder via

```
cd <your-ros-ws/src>
git clone --recursive https://github.com/TooSchoolForCool/Robot-Vision-System.git
```

Then add dependencies specified by .rosinstall using wstool

```
cd <your-ros-ws>
wstool init src
cd src
wstool merge -t . Robot-Vision-System/voxblox-plusplus/voxblox-plusplus_https.rosinstall
wstool update
```

**Step 2: build the project**

First build the ROS packages.

```
cd <your-ros-ws>
catkin build orb_slam2_ros gsm_node map_proc scene_builder
```
Then install the deep-learning-based detection packages (Python 3).

```
workon <your-env>
cd <your-ros-ws>/src/Robot-Vision-System/rp_server
make install
```



