from rp_server import OpenposeServer
from rp_server import DetectronServer


DEFAULT_HOST = "192.168.1.94"
OP_SERVER_PORT = 8800


class Launcher(object):

    def __init__(
        self,
        op_host, op_port,
    ):
        self.op_server_ = OpenposeServer(host=op_host, port=op_port)


    def launch(self):
        self.op_server_.launch()


if __name__ == "__main__":
    launcher = Launcher(
        op_host=DEFAULT_HOST,
        op_port=OP_SERVER_PORT,
    )

    launcher.launch()