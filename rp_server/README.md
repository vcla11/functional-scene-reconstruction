# robot_perception_server

`rp_server` is a python package that provides server-based computer vision functions for robot semantic mapping. 

The output Format of Openpose is available [here](https://github.com/CMU-Perceptual-Computing-Lab/openpose/blob/master/doc/output.md). In this project, we use the [BODY_25](https://github.com/CMU-Perceptual-Computing-Lab/openpose/blob/master/doc/output.md#pose-output-format-body_25) as our pose format.

The output format of Detectron2 is available [here](https://detectron2.readthedocs.io/tutorials/models.html#model-output-format). The catagory information is available in COCO dataset.