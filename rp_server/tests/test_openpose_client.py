import struct

import cv2
import numpy as np

from rp_server import TcpClient
from rp_server import img2bin, bin2img, int2bin
from rp_server import bin2openpose, pack_img



class OpenposeClientTester(object):

    def __init__(self, ip, port):
        self.client_ = TcpClient(ip=ip, port=port)


    def test(self):
        img = cv2.imread("test.jpg")
        data_bin = pack_img(img)
        
        resp_bin = self.client_.send(data_bin)
        resp = bin2openpose(resp_bin)
        
        print(resp)

        

if __name__ == "__main__":
    tester = OpenposeClientTester(ip="192.168.1.94", port=8800)
    tester.test()