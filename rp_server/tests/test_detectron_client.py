import struct
import time

import cv2
import numpy as np

from rp_server import TcpClient
from rp_server import img2bin, bin2img, int2bin
from rp_server import pack_img, bin2detectron

TESTED_IMAGE = [
    "test_complex.png"
    # "wall.jpeg"
]

class DetectronClientTester(object):

    def __init__(self, ip, port, model_type="Inst_seg"):
        self.client_ = TcpClient(ip=ip, port=port)
        self.model_ = model_type


    def test(self):
        for img_file in TESTED_IMAGE:
            start_t = time.time()
            img = cv2.imread(img_file)
            data_bin = pack_img(img)
            
            resp_bin = self.client_.send(data_bin)
            resp = bin2detectron(resp_bin, self.model_)
            
            print("Time elapsed: {}".format(time.time() - start_t))
            print(resp)
        

if __name__ == "__main__":
    tester = DetectronClientTester(ip="192.168.1.94", port=8801, model_type="Pano_seg")
    tester.test()