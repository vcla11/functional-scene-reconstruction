import struct
import time

import cv2
import numpy as np
import torch
import torch.nn

# torch.set_default_tensor_type('torch.cuda.FloatTensor')

import detectron2
from detectron2 import model_zoo
from detectron2.config import get_cfg
from detectron2.engine import DefaultPredictor
from detectron2.modeling import build_model
import detectron2.data.transforms as T
from detectron2.structures import ImageList

from detectron2.checkpoint import DetectionCheckpointer
from detectron2.data import (
    MetadataCatalog,
    build_detection_test_loader,
    build_detection_train_loader,
)

from detectron2.modeling.postprocessing import detector_postprocess, sem_seg_postprocess
from detectron2.modeling.meta_arch.panoptic_fpn import combine_semantic_and_instance_outputs

from detectron2.utils.visualizer import Visualizer
from detectron2.data import MetadataCatalog



CONFIG_FILE = {
    "Pano_seg": "COCO-PanopticSegmentation/panoptic_fpn_R_101_3x.yaml",
    "Inst_seg": "COCO-InstanceSegmentation/mask_rcnn_R_101_FPN_3x.yaml",
    "Obj_detect": "COCO-Detection/faster_rcnn_R_101_FPN_3x.yaml"
}

DETECTION_RES_KEY = {
    "Pano_seg": "panoptic_seg",
    "Inst_seg": "instances",
    "Obj_detect": "proposals"
}
        
TESTED_IMAGE = [
    "data/1-{}.png".format(str(x)) for x in range(8)] + ["data/{}.png".format(str(y+1)) for y in range(18)
    # "test_complex.png"
    # "wall.jpeg"
]



class DetectronWrapper(object):

    def __init__(self, task="Pano_seg", score_thresh=0.5):
        self.task_ = task
        self.cfg = get_cfg()
        self.cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = score_thresh

        try:
            assert(task in CONFIG_FILE)
            cfg_file = CONFIG_FILE[task]
            self.cfg.merge_from_file(model_zoo.get_config_file(cfg_file))
            self.cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url(cfg_file)
        except:
            raise Exception("Do not support type `{}`".format(task))
            
        self.predictor_ = DefaultPredictor(self.cfg)

        self.model = build_model(self.cfg)
        self.model.eval()
        self.metadata = MetadataCatalog.get(self.cfg.DATASETS.TEST[0])
        checkpointer = DetectionCheckpointer(self.model)
        checkpointer.load(self.cfg.MODEL.WEIGHTS)

        self.pixel_mean = torch.Tensor(self.cfg.MODEL.PIXEL_MEAN).view(-1, 1, 1).cuda()
        self.pixel_std = torch.Tensor(self.cfg.MODEL.PIXEL_STD).view(-1, 1, 1).cuda()

        self.transform_gen = T.ResizeShortestEdge(
            [self.cfg.INPUT.MIN_SIZE_TEST, self.cfg.INPUT.MIN_SIZE_TEST], self.cfg.INPUT.MAX_SIZE_TEST
        )

        self.input_format = self.cfg.INPUT.FORMAT
        assert self.input_format in ["RGB", "BGR"], self.input_format

            

    def predict(self, img, index):

        with torch.no_grad():  # https://github.com/sphinx-doc/sphinx/issues/4258
            # Apply pre-processing to image.
            if self.input_format == "RGB":
                # whether the model expects BGR inputs or RGB
                img = img[:, :, ::-1]
            height, width = img.shape[:2]
            image = self.transform_gen.get_transform(img).apply_image(img)
            image = torch.as_tensor(image.astype("float32").transpose(2, 0, 1))

            batched_inputs = [{"image": image, "height": height, "width": width}]



            images = [x["image"].to(self.model.device) for x in batched_inputs]
            images = [(x - self.pixel_mean) / self.pixel_std for x in images]
            images = ImageList.from_tensors(images, self.model.backbone.size_divisibility)
            features = self.model.backbone(images.tensor)

            sem_seg_results, sem_seg_losses = self.model.sem_seg_head(features, None)
            proposals, proposal_losses = self.model.proposal_generator(images, features, None)

            self.features = [features[f] for f in self.model.roi_heads.in_features]
            # print(len(proposals[0].proposal_boxes))
            box_features = self.model.roi_heads.box_pooler(self.features, [x.proposal_boxes for x in proposals])
            # print(box_features.size())
            box_features = self.model.roi_heads.box_head(box_features)
            # print(box_features.size())
            predictions = self.model.roi_heads.box_predictor(box_features)
            pred_instances, _ = self.model.roi_heads.box_predictor.inference(predictions, proposals)

            pred_boxes = [x.pred_boxes for x in pred_instances]
            mask_features = self.model.roi_heads.mask_pooler(self.features, pred_boxes)
            detector_results = self.model.roi_heads.mask_head(mask_features, pred_instances)
            # detector_results = self.model.roi_heads.forward_with_given_boxes(features, pred_instances)
            # detector_results, detector_losses = self.model.roi_heads(images, features, proposals, None)

            processed_results = []
            for sem_seg_result, detector_result, input_per_image, image_size in zip(
            sem_seg_results, detector_results, batched_inputs, images.image_sizes):
                height = input_per_image.get("height", image_size[0])
                width = input_per_image.get("width", image_size[1])
                sem_seg_r = sem_seg_postprocess(sem_seg_result, image_size, height, width)
                detector_r = detector_postprocess(detector_result, height, width)

                processed_results.append({"sem_seg": sem_seg_r, "instances": detector_r})

                if self.model.combine_on:
                    panoptic_r = combine_semantic_and_instance_outputs(
                        detector_r,
                        sem_seg_r.argmax(dim=0),
                        self.model.combine_overlap_threshold,
                        self.model.combine_stuff_area_limit,
                        self.model.combine_instances_confidence_threshold,
                    )
                    processed_results[-1]["panoptic_seg"] = panoptic_r

            v = Visualizer(img, MetadataCatalog.get(self.cfg.DATASETS.TRAIN[0]), scale=1.2)
            v = v.draw_instance_predictions(processed_results[0]["instances"].to("cpu"))
            # cv2.imshow(str(index), v.get_image()[:, :, ::-1])
            cv2.imwrite("{}.png".format(str(index)), v.get_image())

            
            # predictions = self.model.forward([inputs])[0]
        # return predictions[DETECTION_RES_KEY[self.task_]]
        return processed_results[0]["instances"]
        # [DETECTION_RES_KEY[self.task_]]


        # ret = self.predictor_(img)
        # return features

        # return ret[ DETECTION_RES_KEY[self.task_] ]

    def get_features(self, box):
        roi_features = self.model.roi_heads.box_pooler(self.features, box)
        # roi_features = torch.mean(roi_features[0],0)
        roi_features = roi_features.reshape((roi_features.size(3)*roi_features.size(1)*roi_features.size(2)))
        # roi_features = roi_features.reshape((roi_features.size(0)*roi_features.size(1)))

        return roi_features


def extract_feature(file_name, id, detector, class_id):
    img = cv2.imread(file_name)
    resp = detector.predict(img, id)

    pre_class = resp.pred_classes.cpu().detach().numpy().tolist()
    pre_box = resp.pred_boxes
    feature = torch.zeros([0,1])

    if class_id in pre_class:
        index = pre_class.index(class_id)
        box = pre_box[index]
        feature = detector.get_features([box])
    
    return feature

    



if __name__ == "__main__":

    tester = DetectronWrapper("Pano_seg")
    cos = torch.nn.CosineSimilarity(dim=0, eps=1e-6)
    class_id = 56 # chair ID

    feature_obs = extract_feature("data/1-2.png", 0, tester, class_id)
    assert len(feature_obs) != 0

    i = 0
    dist_list = []

    for img_file in TESTED_IMAGE:
        

        print(i)
        dist = 0

        feature_temp = extract_feature(img_file, i, tester, class_id)

        start_t = time.time()
        if not len(feature_temp) == 0:
            dist = cos(feature_obs, feature_temp).cpu().detach().numpy().tolist()
        dist_list.append(dist)

        # end_t = time.time()
        # print(end_t-start_t)


        i = i + 1
    
    print(dist_list)
    dist_list = np.array(dist_list)
    sort_index = np.argsort(dist_list)
    print(sort_index)
    dist_list.sort()
    print(dist_list)


        
            
        
        
        # print("Time elapsed: {}".format(time.time() - start_t))
        # print(resp)











    