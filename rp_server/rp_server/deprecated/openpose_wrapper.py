import os

import pyopenpose as op

import rp_server.assets as assets


ASSETS_FILE_DIR =os.path.abspath(assets.__file__)
ASSETS_DIR = os.path.dirname(ASSETS_FILE_DIR)


class OpenposeWrapper(object):

    def __init__(
        self,
        enable_face=False,
        enable_hand=False
    ):
        # setup openpose configuration parameters
        self.op_params_ = dict()
        self.op_params_["model_folder"] = ASSETS_DIR + "/openpose_models/"
        print(self.op_params_["model_folder"])
        self.op_params_["face"] = enable_face
        self.op_params_["hand"] = enable_hand

        # create openpose detection model
        self.detector_ = op.WrapperPython()
        self.detector_.configure(self.op_params_)
        self.detector_.start()

    
    def detect(self, img):
        """
        Detect body pose

        Args:
            img (cv::Mat): image to be processed

        Returns:
            keypoints ()
        """
        datum = op.Datum()
        datum.cvInputData = img

        self.detector_.emplaceAndPop([datum])

        return datum.poseKeypoints


    def __del__(self):
        self.detector_.stop()


def test_openpose_wrapper():
    import cv2

    detector = OpenposeWrapper(model_folder="../models/")
    
    img = cv2.imread("test.jpg")
    print(img.shape)
    keypoints = detector.detect(img)

    print(keypoints)


if __name__ == "__main__":
    test_openpose_wrapper()