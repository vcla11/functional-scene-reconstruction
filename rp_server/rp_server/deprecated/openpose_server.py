from rp_server import TcpServer
from rp_server import OpenposeWrapper
from rp_server import bin2img, bin2int, int2bin


class OpenposeServer(TcpServer):

    def __init__(self, host, port):
        super(OpenposeServer, self).__init__(host=host, port=port)
        
        self.op_ = OpenposeWrapper()


    def handle_connection_(self, conn, addr):
        conn_id = "{}:{}".format(addr[0], addr[1])
        print('New connection from {}'.format(conn_id))

        while not self.quit_event_.is_set():
            pack_size = conn.recv(4)
            
            # end of Connection
            if not pack_size:
                break

            pack_size = bin2int(pack_size)
            # fetch data package
            data = self.recv_all_(conn, pack_size)

            img = bin2img(data)
            keypoints = self.op_.detect(img)
            resp_bin = self.pack_response_(keypoints)
            print(keypoints)
            
            # send back response
            conn.sendall(resp_bin)

        conn.close()
        print("Connection {}: closed".format(conn_id))

    
    def pack_response_(self, resp):
        data_bin = resp.tostring()
        pack_size = int2bin( len(data_bin) )

        return pack_size + data_bin 
    

    
if __name__ == "__main__":
    server = OpenposeServer(host="192.168.1.94", port=8800)
    server.launch()