import numpy as np
import pandas as pd
import open3d as o3d
from utils import *
import copy
import os.path

mode = "rigid"

if mode == "interactive":
    obj_folder = 'models-OBJ/models-interactive/'
    input_csv_name = 'interactive_objects.csv'
    output_csv_name = 'cad_models_interactive.csv'
    output_meshlab_csv_name = 'cad_transform_interactive.csv'
    obj_class_list = ['Refrigerator', 'Microwave', 'Cabinet']
else:
    obj_folder = 'models-OBJ/models/'
    input_csv_name = 'full_metadata.csv'
    output_csv_name = 'cad_models_rigid.csv'
    output_meshlab_csv_name = 'cad_transform_rigid.csv'
    obj_class_list = ['Chair', 'Table', 'Desk', 'Bench', 'Couch', 'Bed', 'Toilet', 'TV', 'Oven', 'Monitor',
                    'Laptop', 'Backpack', 'Bottle', 'Cup', 'Bowl', 'Keyboard', 'Mouse', 'Book']

path_to_dataset = '/media/muzhi/Data/Dataset/ShapeNetSem/'
aligned_obj_folder = 'models-OBJ/models-aligned/'
output_csv_folder = '../'

save_aligned_mesh = True
write_csv = True
write_csv_for_meshlab = False
visualize = False



def process_data(metadata):
    transform_df = pd.DataFrame([],columns=['instance_id','transform'])
    target_df = pd.DataFrame([],columns=['instance_id','category','aligned_dims','aligned_planes'])
    i = 0
    for index, row in metadata.iterrows():
        instance_category = row['category']
        if is_nan(instance_category):
            continue
        instance_category_list = instance_category.split(',')
        for obj_class in obj_class_list:
            if obj_class in instance_category_list:
                if obj_class == 'Desk':
                    obj_class = 'Table'
                if obj_class == 'Monitor':
                    obj_class = 'TV'
                if mode == "interactive":
                    instance_id = row['fullId']
                else:
                    instance_id = row['fullId'].split('.')[-1]
                up = str_to_np_array(row['up'], '\,', np.dtype(float))
                front = str_to_np_array(row['front'], '\,', np.dtype(float))
                unit = float(row['unit'])
                if is_nan(unit):
                    unit = 1.0
                    # continue
                print(instance_id)
                print(obj_class)
                aligned_planes, aligned_dims, transform = get_aligned_planes(instance_id, unit, up, front, visualize, save_aligned_mesh)
                # aligned_planes = []
                aligned_planes_str = '\,'.join([np_array_to_str(x, '\,') for x in aligned_planes])
                aligned_dims_str = np_array_to_str(aligned_dims, '\,')
                df = pd.DataFrame([[instance_id, obj_class, aligned_dims_str, aligned_planes_str]],
                                        columns=['instance_id','category','aligned_dims','aligned_planes'])
                target_df = target_df.append(df)

                transform_str = np_to_str(transform, ',')
                current_df = pd.DataFrame([[row['fullId'], transform_str]],columns=['instance_id','transform'])
                transform_df = transform_df.append(current_df)

                i = i + 1
                print(i)
        # if i > 30:
        #     break
    return target_df, transform_df


def get_aligned_planes(instance_id, unit, up, front, viz = False, save_mesh = False):
    # Get aligned coordinate
    print(up)
    print(front)
    if up.size > 0: 
        up_axis = np.array(up)
    else:
        up_axis = np.array([0,0,1]) 
    if front.size > 0:
        front_axis = np.array(front)
    else:
        front_axis = np.array([0,-1,0])
    
    # Get rotation to canonical coordinate 
    RC_B = np.eye(3)
    RC_B[:,1] = -front_axis
    RC_B[:,2] = up_axis
    RC_B[:,0] = np.cross(RC_B[:,1], RC_B[:,2])
    RC_B = np.linalg.inv(RC_B)
    

    # Transform the mesh: unit(m), axis-aligned
    mesh = o3d.io.read_triangle_mesh(path_to_dataset + obj_folder + instance_id + '.obj')
    pC_B = -(mesh.get_max_bound()+mesh.get_min_bound())/2

    # mesh.compute_vertex_normals()
    # coor = o3d.geometry.TriangleMesh.create_coordinate_frame()
    # o3d.visualization.draw_geometries([mesh, coor])

    mesh.translate(pC_B)
    mesh.rotate(RC_B, [0,0,0])
    mesh.scale(unit, [0,0,0])
    
    aligned_dims = mesh.get_max_bound()-mesh.get_min_bound()

    # Record applied transform
    transform = np.eye(4)
    transform[0:3,0:3] = RC_B * unit
    transform[0:3,3] = np.dot(RC_B, pC_B) * unit

    if save_mesh:
        # if not os.path.exists(path_to_dataset + aligned_obj_folder + instance_id + '.obj'):
        o3d.io.write_triangle_mesh(path_to_dataset + aligned_obj_folder + instance_id + '.obj', mesh)

    # mesh = mesh.simplify_vertex_clustering(voxel_size=0.01, contraction=o3d.geometry.SimplificationContraction.Average)
    mesh.compute_triangle_normals(True)
    if viz:
        mesh.paint_uniform_color([0.5, 0.5, 0.5])
        o3d.visualization.draw_geometries([mesh])
        # mesh_T = copy.deepcopy(mesh).scale(2, mesh.get_center())
        # mesh_T = copy.deepcopy(mesh).transform(T)
        # mesh_T.paint_uniform_color([0.5, 0.5, 0.5])
        # o3d.visualization.draw_geometries([mesh, mesh_T])

    # Sample point cloud from mesh
    pcd = mesh.sample_points_uniformly(number_of_points=5000)
    
    # Detect planes
    box_area = np.array([aligned_dims[0]*aligned_dims[1], aligned_dims[1]*aligned_dims[2], aligned_dims[0]*aligned_dims[2]])
    sorted_area = np.sort(box_area)
    min_area = (sorted_area[0]+sorted_area[1]+sorted_area[1])/10
    planes = detect_planes_from_mesh(mesh, distance_threshood=0.02, angle_threshood=5, max_iterations=800, min_area=min_area, max_plane_num=10)
    # planes = []
    print('Num of planes:', len(planes))

    if mode == "interactive":
        aligned_planes = []
        for plane in planes:
            if abs(plane[3]) < 0.85 * np.amin(aligned_dims)/2:
                continue
            aligned_planes.append(plane)
        planes = aligned_planes


    if (viz):
        line_set, colors = draw_planes(planes, np.linalg.norm(mesh.get_max_bound()))
        arrow_list = draw_plane_normal(planes, colors)
        o3d.visualization.draw_geometries([pcd]+line_set+arrow_list)
    
    return planes, aligned_dims, transform



if __name__ == "__main__":
    if save_aligned_mesh:
        isdir = os.path.isdir(path_to_dataset + aligned_obj_folder)  
        if not isdir:
            os.mkdir(path_to_dataset + aligned_obj_folder) 
            print('Create output folder')
            
    metadata = pd.read_csv(path_to_dataset + input_csv_name)
    target_df, transform_tf = process_data(metadata)
    print(target_df)
    if write_csv:
        target_df.to_csv(output_csv_folder + output_csv_name,sep=',',index=False)

    if write_csv_for_meshlab:
        transform_tf.to_csv(output_csv_folder + output_meshlab_csv_name,sep=',',index=False)